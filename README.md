# 通过内网穿透实现子域名直达内网服务

#### 介绍
Windows 环境下使用 FRP 进行内网穿透，并配置 Nginx 绑定子域名转发内网映射出的服务。

#### 核心轮子
- FRP 
- 公网服务器
- Nginx

#### 思维导图
![](MindMap.png "MindMap")

#### 安装教程

1.  将 [server] 拷贝至公网服务器上，配置 frps.ini
2.  将 [client] 拷贝至公网服务器上，配置 frpc.ini
3.  配置 [Nginx] conf 文件后，在公网服务器 Nginx 目录下更新配置文件，重新载入即可

#### 启动说明

1.  公网机器运行 frp-server-start-up
2.  内网机器运行 frp-client-start-up

#### 拓展说明

1. 可在 [C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp] 路径下生成脚本的快捷方式实现开机自启动